# se-hiring-questions

## Task 1
Download ex1.tar.gz file.

Write a 1 liner or a script that finds inside downloaded tar.gz all the files that start with #### but their content does not contain the name John. This 1 liner or script should also generate an output file with all names found inside #### files, names should be deduplicated and sorted alphabetically.

## Task 2 
Download file ex2.metadata.json

Remap downloaded JSON file to the following structure:
```jsonc
{
      "classes_with_virtual_attributes": [
        {
            "name": "Applications",
            "attributes": [
                {
                    "name": "import_id",
                    "id": 1028
                },
                {
                    "name": "APPID_LOB",
                    "id": 1029
                }
                /*...*/
            ]
        },
        {
            "name": "People",
            "attributes": [
                {
                    "name": "FIRST_LAST",
                    "id": 974
                },
                {
                    "name": "PRIMARY_ADDRESS",
                    "id": 973
                }
                /*...*/
            ]
        }
 
    ]
}
```

Where:
* `classes_with_virtual_attributes[].name` is the name of the classes that has any attribute marked as `"virtual": true` (example line 2487).
* `classes_with_virtual_attributes[].attributes[].name` is the name of the virtual attribute
* `classes_with_virtual_attributes[].attributes[].id` is the id of the virtual attribute from the referenced class.

## Task 3
Please see the two attached Excel files with some data. The offense data is from a police department. The police department wants to analyze this data to look for patterns, crime rates, etc. They want to know what types of crime are on the rise, what areas of town are the biggest problem areas, changes over time, etc as well as any other insights you may have to offer. 

The call detail record data is from an ongoing investigation. Detectives want to analyze this to find patterns of movement, frequented areas, and any other information you can provide about the subject.

These two datasets should be worked on as separate tasks. Analyze the data however you'd choose, make visualizations, and use any external tools you'd like. Approach this like an analytical product that you are going to present to a customer
